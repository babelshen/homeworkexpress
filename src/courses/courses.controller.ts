import { Request, Response } from 'express';
import * as coursesService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from './types/course-create-request.interface';
import { HttpStatuses } from '../application/enums/http.statuses.enums';

export const getAllCourses = async (request: Request, response: Response) => {
  const courses = await coursesService.getAllCourses();
  response.json(courses);
};

export const getCourseById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.getCourseById(id);
  response.json(course);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await coursesService.createCourse(request.body);
  response.status(HttpStatuses.CREATED).json(course);
};

export const getCourseMark = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.getCourseMarks(id);
  response.json(course);
};
