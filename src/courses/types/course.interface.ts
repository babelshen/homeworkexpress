export interface ICourse {
  id: number;
  name: string;
  description: string;
  hours: number;
}

export interface IMarkOfCourse {
  courseName: string;
  lectorName: string;
  studentName: string;
  studentSurname: string;
  mark: number;
}
