import { Router } from 'express';
import * as coursesController from './courses.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middleware/validation.middleware';
import { courseSchema } from './courseSchema';
import { idSchema } from '../application/schemas/id-param.schemas';

const courseRouter = Router();

courseRouter.get(
  '/:id',
  validator.params(idSchema),
  controllerWrapper(coursesController.getCourseById),
);

courseRouter.get('/', controllerWrapper(coursesController.getAllCourses));

courseRouter.get(
  '/:id/marks',
  validator.params(idSchema),
  controllerWrapper(coursesController.getCourseMark),
);

courseRouter.post(
  '/',
  validator.body(courseSchema),
  controllerWrapper(coursesController.createCourse),
);

export default courseRouter;
