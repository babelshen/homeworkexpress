import { HttpStatuses } from '../application/enums/http.statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from './entities/course.entity';
import { ICourse, IMarkOfCourse } from './types/course.interface';

const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
  try {
    const courses = await coursesRepository.find();
    if (!courses.length) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'No courses in the database',
      );
    }
    return courses;
  } catch (e) {
    throw e;
  }
};

export const getCourseById = async (id: number): Promise<Course> => {
  try {
    const course = await coursesRepository.findOne({
      where: {
        id,
      },
    });

    if (!course) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
    }

    return course;
  } catch (e) {
    throw e;
  }
};

export const createCourse = async (
  courseCreateSchema: Omit<ICourse, 'id'>,
): Promise<Course> => {
  try {
    const course = await coursesRepository.findOne({
      where: {
        name: courseCreateSchema.name,
      },
    });

    if (course) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'Course with this name already exists',
      );
    }

    return coursesRepository.save(courseCreateSchema);
  } catch (e) {
    throw e;
  }
};

export const getCourseMarks = async (id: number): Promise<IMarkOfCourse[]> => {
  try {
    const courses = await coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.marks', 'mark')
      .leftJoinAndSelect('mark.lector', 'lector')
      .leftJoinAndSelect('mark.student', 'student')
      .select([
        'course.name AS "courseName"',
        'lector.name AS "lectorName"',
        'student.name AS "studentName"',
        'student.surname AS "studentSurname"',
        'mark.mark AS mark',
      ])
      .where('course.id = :id', { id })
      .getRawMany();

    if (!courses.length) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Courses not found');
    }

    return courses;
  } catch (e) {
    throw e;
  }
};
