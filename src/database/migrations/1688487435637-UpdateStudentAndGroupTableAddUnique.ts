import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateStudentAndGroupTableAddUnique1688487435637
  implements MigrationInterface
{
  name = 'UpdateStudentAndGroupTableAddUnique1688487435637';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "UQ_25985d58c714a4a427ced57507b" UNIQUE ("email")`,
    );
    await queryRunner.query(
      `ALTER TABLE "groups" ADD CONSTRAINT "UQ_664ea405ae2a10c264d582ee563" UNIQUE ("name")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "groups" DROP CONSTRAINT "UQ_664ea405ae2a10c264d582ee563"`,
    );
    await queryRunner.query(
      `ALTER TABLE "students" DROP CONSTRAINT "UQ_25985d58c714a4a427ced57507b"`,
    );
  }
}
