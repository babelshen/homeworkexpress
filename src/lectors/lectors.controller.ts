import { Request, Response } from 'express';
import * as lectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';
import { HttpStatuses } from '../application/enums/http.statuses.enums';
import { ILectorAddCourseRequest } from './types/lector-add-course-request.interface';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.getLectorById(id);
  response.json(lector);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body);
  response.status(HttpStatuses.CREATED).json(lector);
};

export const addCourseForLector = async (
  request: ValidatedRequest<ILectorAddCourseRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.addCourseForLector(id, request.body.courseId);
  response.json(lector);
};

export const getCourseOfLector = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await lectorsService.getCourseOfLector(id);
  response.json(course);
};
