import Joi from 'joi';
import { ILector } from './types/lectors.interface';

export const lectorSchema = Joi.object<Omit<ILector, 'id'>>({
  name: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export const addCourseForLectorSchema = Joi.object<{ courseId: number }>({
  courseId: Joi.number().required(),
});
