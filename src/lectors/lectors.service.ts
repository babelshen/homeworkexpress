import { HttpStatuses } from '../application/enums/http.statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { getCourseById } from '../courses/courses.service';
import { Lector } from './entities/lector.entity';
import { ICourseOfLector, ILector } from './types/lectors.interface';

const lectorsRepository = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
  try {
    const lectors = await lectorsRepository.find();
    if (!lectors.length) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'No lectors in the database',
      );
    }
    return lectors;
  } catch (e) {
    throw e;
  }
};

export const getLectorById = async (id: number): Promise<Lector> => {
  try {
    const lector = await lectorsRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    return lector;
  } catch (e) {
    throw e;
  }
};

export const createLector = async (
  lectorCreateSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  try {
    const lector = await lectorsRepository.findOne({
      where: {
        email: lectorCreateSchema.email,
      },
    });

    if (lector) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'Lector with this e-mail already exists',
      );
    }

    return lectorsRepository.save(lectorCreateSchema);
  } catch (e) {
    throw e;
  }
};

export const addCourseForLector = async (
  lectorId: number,
  courseId: number,
): Promise<Lector> => {
  try {
    const lector = await lectorsRepository.findOne({
      where: {
        id: lectorId,
      },
      relations: ['courses'],
    });

    if (!lector) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    const course = await getCourseById(courseId);
    if (!course) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
    }

    const doubleCourse = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoin('lector.courses', 'course')
    .where('lector.id = :lectorId', { lectorId })
    .andWhere('course.id = :courseId', { courseId })
    .getOne();

    if (doubleCourse) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'The course has already been added to the lecturer',
      );
    }

    lector.courses.push(course);
    return lectorsRepository.save(lector);
  } catch (e) {
    throw e;
  }
};

export const getCourseOfLector = async (
  id: number,
): Promise<ICourseOfLector[]> => {
  try {
    const lector = await lectorsRepository
      .createQueryBuilder('lector')
      .leftJoin('lector.courses', 'course')
      .select([
        'lector.name AS "lectorName"',
        'course.id AS "courseId"',
        'course.name AS "courseName"',
      ])
      .where('lector.id = :id', { id })
      .getRawMany();

    if (!lector.length) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    return lector;
  } catch (e) {
    throw e;
  }
};
