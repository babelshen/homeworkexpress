export interface ILector {
  id: number;
  name: string;
  email: string;
  password: string;
}

export interface ICourseOfLector {
  lectorName: string;
  courseId: number;
  courseName: string;
}
