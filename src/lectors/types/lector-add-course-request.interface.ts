import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

export interface ILectorAddCourseRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: {
    courseId: number;
  };
}
