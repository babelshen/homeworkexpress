import { Router } from 'express';
import * as lectorsController from './lectors.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middleware/validation.middleware';
import { addCourseForLectorSchema, lectorSchema } from './lector.schemas';
import { idSchema } from '../application/schemas/id-param.schemas';

const lectorRouter = Router();

lectorRouter.get(
  '/:id',
  validator.params(idSchema),
  controllerWrapper(lectorsController.getLectorById),
);

lectorRouter.get('/', controllerWrapper(lectorsController.getAllLectors));

lectorRouter.get(
  '/:id/courses',
  validator.params(idSchema),
  controllerWrapper(lectorsController.getCourseOfLector),
);

lectorRouter.post(
  '/',
  validator.body(lectorSchema),
  controllerWrapper(lectorsController.createLector),
);

lectorRouter.patch(
  '/:id/course',
  validator.params(idSchema),
  validator.body(addCourseForLectorSchema),
  controllerWrapper(lectorsController.addCourseForLector),
);

export default lectorRouter;
