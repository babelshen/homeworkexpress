import Joi from 'joi';
import { IGroup } from './types/groups.interface';

export const groupSchema = Joi.object<Omit<IGroup, 'id'>>({
  name: Joi.string().required(),
});
