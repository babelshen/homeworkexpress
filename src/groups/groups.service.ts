import { UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http.statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import { IGroup } from './types/groups.interface';
import { updateStudentById } from '../students/students.service';

const groupsRepository = AppDataSource.getRepository(Group);

export const getGroupById = async (id: number): Promise<Group> => {
  try {
    const group = await groupsRepository.findOne({
      where: {
        id,
      },
    });

    if (!group) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
    }

    return group;
  } catch (e) {
    throw e;
  }
};

export const getGroupByIdWithStudents = async (id: number): Promise<Group> => {
  try {
    const group = await groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .where('group.id = :id', { id })
      .getOne();

    if (!group) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
    }

    return group;
  } catch (e) {
    throw e;
  }
};

export const getAllGroupsWithStudents = async (): Promise<Group[]> => {
  try {
    const groups = await groupsRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .getMany();

    if (!groups.length) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'No groups in the database',
      );
    }

    return groups;
  } catch (e) {
    throw e;
  }
};

export const createGroup = async (
  groupCreateSchema: Omit<IGroup, 'id'>,
): Promise<Group> => {
  try {
    return await groupsRepository.save(groupCreateSchema);
  } catch (e) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exists',
    );
  }
};

export const updateGroupById = async (
  id: number,
  groupUpdateSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  try {
    const group = await getGroupById(id);
    if (group) {
      const result = await groupsRepository.update(id, groupUpdateSchema);
      return result;
    } else {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
    }
  } catch (e) {
    throw e;
  }
};

export const deleteGroupById = async (id: number): Promise<void> => {
  try {
    const group = await getGroupByIdWithStudents(id);

    if (!group) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
    }
    const students = group.students;
    if (students.length > 0) {
      await Promise.all(
        students.map((student) => {
          return updateStudentById(student.id, { groupId: null });
        }),
      );
    }

    await groupsRepository.delete(id);
  } catch (e) {
    throw e;
  }
};
