import { Router } from 'express';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middleware/validation.middleware';
import { groupSchema } from './groups.schemas';
import { idSchema } from '../application/schemas/id-param.schemas';

const groupRouter = Router();

groupRouter.get(
  '/:id',
  validator.params(idSchema),
  controllerWrapper(groupsController.getGroupByIdWithStudents),
);

groupRouter.get(
  '/',
  controllerWrapper(groupsController.getAllGroupWithStudents),
);

groupRouter.post(
  '/',
  validator.body(groupSchema),
  controllerWrapper(groupsController.createGroup),
);

groupRouter.patch(
  '/:id',
  validator.params(idSchema),
  validator.body(groupSchema),
  controllerWrapper(groupsController.updateGroupById),
);

groupRouter.delete(
  '/:id',
  validator.params(idSchema),
  controllerWrapper(groupsController.deleteGroupById),
);

export default groupRouter;
