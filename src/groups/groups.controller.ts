import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as groupsService from './groups.service';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { HttpStatuses } from '../application/enums/http.statuses.enums';

export const getAllGroupWithStudents = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroupsWithStudents();
  response.json(groups);
};

export const getGroupByIdWithStudents = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.getGroupByIdWithStudents(id);
  response.json(group);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response.status(HttpStatuses.CREATED).json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.json(group);
};

export const deleteGroupById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.status(HttpStatuses.NO_CONTENT).json(group);
};
