import { Column, Entity, OneToMany, Unique } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entities';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'groups' })
export class Group extends CoreEntity {
  @Unique(['name'])
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @OneToMany(() => Student, (student) => student.group)
  students: Student[];
}
