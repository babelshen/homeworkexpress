import { Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as marksService from './marks.service';
import { HttpStatuses } from '../application/enums/http.statuses.enums';
import { IMarkAddRequest } from './types/mark-add-request.interface';

export const addMark = async (
  request: ValidatedRequest<IMarkAddRequest>,
  response: Response,
) => {
  const mark = await marksService.addMark(request.body);
  response.status(HttpStatuses.CREATED).json(mark);
};
