import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IMark } from './marks.interface';

export interface IMarkAddRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: IMark & {
    lector_id: number;
    course_id: number;
    student_id: number;
  };
}
