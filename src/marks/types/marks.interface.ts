export interface IMark {
  id: number;
  mark: number;
}

export interface IMarkSchema extends IMark {
  lector_id: number;
  course_id: number;
  student_id: number;
}
