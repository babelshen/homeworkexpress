import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entities';
import { Course } from '../../courses/entities/course.entity';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @ManyToOne(() => Course, (course) => course.marks)
  @JoinColumn({ name: 'course_id' })
  course: number;

  @ManyToOne(() => Student, (student) => student.marks)
  @JoinColumn({ name: 'student_id' })
  student: number;

  @ManyToOne(() => Lector, (lector) => lector.marks)
  @JoinColumn({ name: 'lector_id' })
  lector: number;

  @Column({
    type: 'integer',
    nullable: true,
  })
  mark: number;
}
