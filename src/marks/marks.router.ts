import { Router } from 'express';
import * as marksController from './marks.controller';
import validator from '../application/middleware/validation.middleware';
import controllerWrapper from '../application/utilities/controller-wrapper';
import { markAddRequestSchema } from './marks.schema';

const markRouter = Router();

markRouter.post(
  '/',
  validator.body(markAddRequestSchema),
  controllerWrapper(marksController.addMark),
);

export default markRouter;
