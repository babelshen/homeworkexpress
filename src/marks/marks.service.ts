import { DeleteResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http.statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { getCourseById } from '../courses/courses.service';
import { getLectorById } from '../lectors/lectors.service';
import { getStudentById } from '../students/students.service';
import { Mark } from './entities/mark.entity';
import { IMarkSchema } from './types/marks.interface';

const marksRepository = AppDataSource.getRepository(Mark);

export const addMark = async (markSchema: IMarkSchema): Promise<Mark> => {
  try {
    const lector = await getLectorById(markSchema.lector_id);

    if (!lector) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    const course = await getCourseById(markSchema.course_id);
    if (!course) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
    }

    const lectorHasCourse = lector.courses.find(
      (courseItem) => courseItem.id === course.id,
    );

    if (!lectorHasCourse) {
      throw new HttpException(
        HttpStatuses.BAD_REQUEST,
        'The lecturer does not have a specified course',
      );
    }
    
    await getStudentById(markSchema.student_id);

    const mark = new Mark();
    mark.course = markSchema.course_id;
    mark.student = markSchema.student_id;
    mark.lector = markSchema.lector_id;
    mark.mark = markSchema.mark;
    return marksRepository.save(mark);
  } catch (e) {
    throw e;
  }
};

export const deleteMarksByStudentId = async (
  id: number,
): Promise<DeleteResult> => {
  try {
    const result = await marksRepository.delete({ student: id });

    if (!result.affected) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Mark not found');
    }

    return result;
  } catch (e) {
    throw e;
  }
};
