import Joi from 'joi';

export const markAddRequestSchema = Joi.object({
  mark: Joi.number().min(1).max(12).required(),
  lector_id: Joi.number().required(),
  course_id: Joi.number().required(),
  student_id: Joi.number().required(),
});
