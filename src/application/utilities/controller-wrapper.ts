import { NextFunction, Request, Response } from 'express';

const controllerWrapper = (requestHandler: any) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await requestHandler(req, res, next);
    } catch (e) {
      next(e);
    }
  };
};

export default controllerWrapper;
