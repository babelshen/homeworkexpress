import Joi from 'joi';

export const idSchema = Joi.object<{ id: number }>({
  id: Joi.number().required(),
});
