import { NextFunction, Response, Request } from 'express';

export const notFoundPageError = (req: Request, res: Response) => {
  const status = 404;
  const message = 'Page Not Found';
  res.status(status).send({ status, message });
};
