import { NextFunction, Response, Request } from 'express';
import HttpException from '../exceptions/http-exception';
import { HttpStatuses } from '../enums/http.statuses.enums';

const exceptionsFilter = (
  e: HttpException,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const status = e.status || HttpStatuses.INTERNAL_SERVER_ERROR;
  const message = e.message || 'Something wrong ...';
  res.status(status).send({ status, message });
};

export default exceptionsFilter;
