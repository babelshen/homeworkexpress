import basicAuth from 'express-basic-auth';
import HttpException from '../exceptions/http-exception';
import { HttpStatuses } from '../enums/http.statuses.enums';

const username = process.env.APP_USERNAME as string;
const password = process.env.PASSWORD as string;

const auth = basicAuth({
  users: { [username]: password },
});

export default auth;
