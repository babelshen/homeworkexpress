import express from 'express';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import studentRouter from '../students/students.router';
import groupRouter from '../groups/groups.router';
import exceptionsFilter from './middleware/exceptions.filter';
import { notFoundPageError } from './middleware/error.404handler.middleware';
import auth from './middleware/auth.middleware';
import { AppDataSource } from '../configs/database/data-source';
import lectorRouter from '../lectors/lectors.router';
import courseRouter from '../courses/courses.router';
import markRouter from '../marks/marks.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());
// app.use(auth);

AppDataSource.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch((error) => {
    console.log('Error: ', error);
  });

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));

app.use('/api/v1/students', studentRouter);
app.use('/api/v1/groups', groupRouter);
app.use('/api/v1/lectors', lectorRouter);
app.use('/api/v1/courses', courseRouter);
app.use('/api/v1/marks', markRouter);

app.use(notFoundPageError);
app.use(exceptionsFilter);

export default app;
