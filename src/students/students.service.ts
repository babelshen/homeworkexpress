import ObjectID from 'bson-objectid';
import path from 'path';
import fs from 'fs/promises';
import { DeleteResult, UpdateResult } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { HttpStatuses } from '../application/enums/http.statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import {
  IRespondMarksOfStudent,
  IRespondStudent,
  IStudent,
} from './types/students.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { getGroupById } from '../groups/groups.service';
import { deleteMarksByStudentId } from '../marks/marks.service';

const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  try {
    const students = studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'group.name as "groupName"',
        'student.imagePath as "imagePath"',
      ])
      .leftJoin('student.group', 'group');

    if (name) {
      students.where('student.name = :name', { name });
    }

    const result = await students.getRawMany();

    if (!result.length) {
      if (name) {
        throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
      } else {
        throw new HttpException(
          HttpStatuses.BAD_REQUEST,
          'No students in the database',
        );
      }
    }

    return result;
  } catch (e) {
    throw e;
  }
};

export const getStudentById = async (id: number): Promise<Student> => {
  try {
    const student = await studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .getRawOne();

    if (!student) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return student;
  } catch (e) {
    throw e;
  }
};

export const addGroupForStudent = async (
  studentId: number,
  groupId: number,
): Promise<Student> => {
  try {
    const student = await studentsRepository.findOne({
      where: {
        id: studentId,
      },
    });

    if (!student) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    const group = await getGroupById(groupId);
    if (!group) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
    }

    student.group = group;
    return studentsRepository.save(student);
  } catch (e) {
    throw e;
  }
};

export const createStudent = async (
  studentCreateSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  try {
    return await studentsRepository.save(studentCreateSchema);
  } catch (e) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }
};

export const updateStudentById = async (
  id: number,
  studentUpdateSchema: Partial<IRespondStudent>,
): Promise<UpdateResult> => {
  try {
    const result = await studentsRepository.update(
      id,
      studentUpdateSchema as QueryDeepPartialEntity<Student>,
    );

    if (!result.affected) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return result;
  } catch (e) {
    throw e;
  }
};

export const deleteStudentById = async (id: number): Promise<DeleteResult> => {
  try {
    await getStudentById(id);
    
    const marks = await getStudentMarks(id);
    if (marks[0]?.mark) {
      await deleteMarksByStudentId(id);
    }

    const result = await studentsRepository.delete(id);

    if (!result.affected) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return result;
  } catch (e) {
    throw e;
  }
};

export const addImage = async (
  id: number,
  filePath?: string,
): Promise<Student> => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtention = path.extname(filePath);
    const imageName = imageId + imageExtention;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(
      __dirname,
      '../',
      'public',
      studentsDirectoryName,
    );

    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const student = await studentsRepository.findOne({
      where: {
        id,
      },
    });
    if (!student) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }
    student.imagePath = imagePath;
    await studentsRepository.save(student);

    return student;
  } catch (e) {
    await fs.unlink(filePath);
    throw e;
  }
};

export const getStudentMarks = async (
  id: number,
): Promise<IRespondMarksOfStudent[]> => {
  try {
    const student = await studentsRepository
      .createQueryBuilder('student')
      .leftJoinAndSelect('student.marks', 'mark')
      .leftJoinAndSelect('mark.course', 'course')
      .select([
        'student.name AS "studentName"',
        'student.surname AS "studentSurname"',
        'course.name AS "courseName"',
        'mark.mark AS mark',
      ])
      .where('student.id = :id', { id })
      .getRawMany();

    if (!student.length) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    return student;
  } catch (e) {
    throw e;
  }
};
