export interface IStudent {
  id: number;
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath?: string;
}

export interface IRespondStudent extends IStudent {
  groupId: number | null;
}

export interface IRespondMarksOfStudent {
  studentName: string;
  studentSurname: string;
  courseName: string;
  mark: number;
}
