import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IRespondStudent } from './students.interface';

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IRespondStudent>;
}
