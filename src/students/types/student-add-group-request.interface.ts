import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

export interface IStudentAddGroupRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: {
    groupId: number;
  };
}
