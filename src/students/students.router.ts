import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middleware/validation.middleware';
import {
  studentCreateSchema,
  studentUpdateSchema,
  studentAddGroup,
} from './students.schema';
import { idSchema } from '../application/schemas/id-param.schemas';
import uploadMiddleware from '../application/middleware/upload.middleware';

const studentRouter = Router();

studentRouter.get('/', controllerWrapper(studentsController.getAllStudents));
studentRouter.get(
  '/:id',
  validator.params(idSchema),
  controllerWrapper(studentsController.getStudentById),
);

studentRouter.get(
  '/:id/marks',
  validator.params(idSchema),
  controllerWrapper(studentsController.getStudentMarks),
);

studentRouter.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);
studentRouter.patch(
  '/:id',
  validator.params(idSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);

studentRouter.patch(
  '/:id/group',
  validator.params(idSchema),
  validator.body(studentAddGroup),
  controllerWrapper(studentsController.addGroupForStudent),
);

studentRouter.delete(
  '/:id',
  validator.params(idSchema),
  controllerWrapper(studentsController.deleteStudentById),
);

studentRouter.patch(
  '/:id/image',
  validator.params(idSchema),
  uploadMiddleware.single('file'),
  controllerWrapper(studentsController.addImage),
);

export default studentRouter;
