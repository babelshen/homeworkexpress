import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as studentsService from './students.service';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentAddGroupRequest } from './types/student-add-group-request.interface';
import { HttpStatuses } from '../application/enums/http.statuses.enums';

export const getAllStudents = async (
  request: Request<{ name?: string }>,
  response: Response,
) => {
  const name = request.query.name as string;
  let students;

  if (name) {
    students = await studentsService.getAllStudents(name);
  } else {
    students = await studentsService.getAllStudents();
  }

  response.json(students);
};

export const getStudentById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.json(student);
};

export const addGroupForStudent = async (
  request: ValidatedRequest<IStudentAddGroupRequest>,
  response: Response,
) => {
  const { id } = request.params;
  let student = await studentsService.addGroupForStudent(id, request.body.groupId);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.status(HttpStatuses.CREATED).json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  let student = await studentsService.updateStudentById(id, request.body);
  response.json(student);
};

export const deleteStudentById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  response.status(HttpStatuses.NO_CONTENT).json(student);
};

export const getStudentMarks = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentMarks(id);
  response.json(student);
};

export const addImage = async (
  request: Request<{ id: number; file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const student = await studentsService.addImage(id, path);
  response.json(student);
};
