import Joi from 'joi';
import { IRespondStudent, IStudent } from './types/students.interface';

export const studentCreateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().required(),
  surname: Joi.string().required(),
  email: Joi.string().required(),
  age: Joi.number().required(),
});

export const studentUpdateSchema = Joi.object<Partial<IRespondStudent>>({
  name: Joi.string().optional(),
  surname: Joi.string().optional(),
  email: Joi.string().optional(),
  age: Joi.number().optional(),
  groupId: Joi.number().allow(null).optional(),
});

export const studentAddGroup = Joi.object<{ groupId: number }>({
  groupId: Joi.number().required(),
});
