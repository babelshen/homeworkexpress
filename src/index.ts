import 'dotenv/config';
import app from './application/app';

const port = process.env.PORT;

const startServer = async () => {
  app.listen(port, () => {
    console.log(`Server started at ${port}`);
  });
};

startServer();
